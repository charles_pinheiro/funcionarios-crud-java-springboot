package com.backend.funcionarios.repo;

import com.backend.funcionarios.models.Funcionario;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FuncionarioRepo extends JpaRepository<Funcionario, Long> {

    Funcionario findById(long id);
}
